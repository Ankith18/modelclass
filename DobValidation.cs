using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MedicineMonitoring.Models
{
    public class DobValidation : ValidationAttribute
    {

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                DateTime d = Convert.ToDateTime(value);
                if (d != null)
                {
                    if (d < DateTime.UtcNow)
                    {
                        return ValidationResult.Success;
                    }
                }
            }
            return new ValidationResult(ErrorMessage ?? "Please provide proper DOB");
        }

    }
    public class BrachIdValidation : ValidationAttribute
    {

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            User user = new User();
            if (user.Usercategory.Equals(Enum.GetName(typeof(UserCategory), 2)))
            {
                if (value == null)
                {
                    return new ValidationResult(ErrorMessage ?? "Branch Id Required");
                }

            }
            return ValidationResult.Success;
        }           
    }
}
