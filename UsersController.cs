﻿using MedicineMonitoring.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MedicineMonitoring.Controllers
{
    public class UsersController : Controller
    {
       
        // GET: Users
        public void Save(UserContext userContext)
        {
            userContext.SaveChanges();
        }
        public ActionResult Index()
        {
            using (UserContext _dbcontext = new UserContext())
            {

                return View(_dbcontext.Users.ToList());
            }
        }
       
        public ActionResult Registration(int? id)
        {
           
            var tuple = new Tuple<User, Branch>(new User(),new Branch());
            ViewBag.userType = id;
            
            return View(tuple);
        }
        [HttpPost]
        public ActionResult Registration(int? id,User Item1,Branch Item2)
        {
            using (UserContext _dbcontext = new UserContext())
            {

                if (!ModelState.IsValid)
                {
                    return View();
                }
                else
                {
                    if (Enum.GetName(typeof(UserCategory), id) == Enum.GetName(typeof(UserCategory), 2))
                    {
                        Item1.Usercategory = (UserCategory)id;
                        Item1.Approve = false;
                        Item1.BranchId = Item2.BranchId;
                        _dbcontext.Branches.Add(Item2);
                        _dbcontext.Users.Add(Item1);
                        Save(_dbcontext);
                        return View();

                    }
                    else
                    {
                        Item1.Usercategory = (UserCategory)id;
                        Item1.Approve = true;
                        Item1.BranchId = null;
                        _dbcontext.Users.Add(Item1);
                        Save(_dbcontext);
                        return View();
                    }
                }
            }
            return View();
        }
    }
}