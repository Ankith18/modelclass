using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MedicineMonitoring.Models
{
    public class Branch
    {
        [Key]
        public string BranchId { get; set; }

        [Required(ErrorMessage = "Branch Name is required")]
        [StringLength(20, MinimumLength = 3)]
        [Display(Name = "Branch Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Enter Branch Address")]
        [StringLength(100)]
        [Display(Name = "Branch Address")]
        public string Address { get; set; }

      
    }
}
